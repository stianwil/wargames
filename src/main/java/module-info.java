module WarGames {
    requires javafx.fxml;
    requires java.desktop;
    requires javafx.graphics;
    requires javafx.controls;
    exports edu.ntnu.idatt2001.WarGame.Controllers;
    exports edu.ntnu.idatt2001.WarGame.App;
}