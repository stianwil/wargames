package edu.ntnu.idatt2001.WarGame.Army;
import edu.ntnu.idatt2001.WarGame.ENUMS.Unit_Type;
import edu.ntnu.idatt2001.WarGame.Units.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Army.
 */
public class Army {

    private String name;
    private ArrayList<Unit> units;
    public ArrayList<String> unitsName;

    /**
     * Instantiates a new Army containing units.
     *
     * @param name the name of the army.
     */
    public Army(String name) {
        if (name.isBlank()) throw new IllegalArgumentException("The name of an army cannot be empty");
        this.name=name;
        this.units = new ArrayList<>();
    }

    /**
     * Instantiates a new Army.
     *
     * @param name  the name of the army
     * @param units the units of the army
     */
    public Army(String name, ArrayList<Unit> units) {
        if (name.isBlank()) throw new IllegalArgumentException("The name of an army cannot be empty");
        this.name=name;
        this.units=units;
    }

    /**
     * Add unit to the list of units.
     *
     * @param unit the unit to be added.
     */
    public void addUnit(Unit unit) {
        if(unit.getHealth()!=0) {
            this.units.add(unit);
        }
    }

    /**
     * Add all units.
     *
     * @param units the units in the units list.
     */
    public void addAllUnits(ArrayList<Unit> units) {
        for (Unit u : units) {
            addUnit(u);
        }
    }

    /**
     * Gets name of the army.
     *
     * @return the name of the army.
     */
    public String getName() {
        return name;
    }

    /**
     * Remove unit if it is dead.
     * If unit is dead, removes it and trims the units size list.
     *
     * @param unit the unit to be removed
     */
    public void removeDeadUnit(Unit unit) {
        if (unit.getHealth() == 0) {
            this.units.remove(unit);
        }
        units.trimToSize();
    }

    /**
     * Removes unit from the units list.
     * Then trims the units list.
     * @param unit to be removed.
     */
    public void removeUnit(Unit unit) {
        this.units.remove(unit);
        units.trimToSize();
    }

    /**
     * Checks if the units list contains units.
     *
     * @return the boolean
     */
    public boolean hasUnits() {
        if (units.size() == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Gets random unit in the units list.
     *
     * @return the random unit.
     */
    public Unit getRandom() {
        Random ran = new Random();
        return units.get(ran.nextInt(units.size()));
    }

    /**
     * Method to get all instances of Infantryunits in the army
     * @return a list of InfantryUnits
     */
    public ArrayList<Unit> getInfantryUnit() {
        return (ArrayList<Unit>) units.stream().filter(u -> u instanceof InfantryUnit).collect(Collectors.toList());
    }

    /**
     * Method to get all instances of RangedUnits in the army
     * @return a list of RangedUnits
     */
    public ArrayList<Unit> getRangedUnit() {
        return (ArrayList<Unit>) units.stream().filter(u -> u instanceof RangedUnit).collect(Collectors.toList());
    }

    /**
     * Method to get all instances of CavalryUnits in the army
     * @return a list of CavalryUnits
     */
    public ArrayList<Unit> getCavalryUnit() {
        return (ArrayList<Unit>) units.stream().filter(u -> u instanceof CavalryUnit).filter(u -> !(u instanceof CommanderUnit)).collect(Collectors.toList());
    }

    /**
     * Method to get all instances of CommanderUnits in the army
     * @return a list of CommanderUnits
     */
    public ArrayList<Unit> getCommanderUnit() {
        return (ArrayList<Unit>) units.stream().filter(u -> u instanceof CommanderUnit).collect(Collectors.toList());
    }

    /**
     * Gets all units.
     *
     * @return all units
     */
    public ArrayList<Unit> getAllUnits() {
        return this.units;
    }

    /**
     * Returns a string of the army and its units.
     * @return units
     */
    @Override
    public String toString() {
        return "Army: \n " + units;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }



    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }
}
