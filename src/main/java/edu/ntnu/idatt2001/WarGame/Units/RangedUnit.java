package edu.ntnu.idatt2001.WarGame.Units;

import edu.ntnu.idatt2001.WarGame.ENUMS.Terrain;

/**
 * The type Ranged unit.
 */
public class RangedUnit extends Unit {
    private int timesAttacked;

    /**
     * Instantiates a new Ranged unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
        int timesAttacked = 0;
    }



    /**
     * Instantiates a new Ranged unit.
     *
     * @param name   the name
     * @param health the health
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
        timesAttacked = 0;
    }

    /**
     * Gets bonus attack damage if fighting in the hills, and gets lower attack damage if fighting in the forest.
     * @param terrain
     * @return
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int attackBonus = 3;
        if (terrain.equals(Terrain.HILL)) {
            attackBonus += 2;
        }

        if (terrain.equals(Terrain.FOREST)) {
            attackBonus -= 2;
        }
        return attackBonus;
        }

    /**
     * Gets less resistance after each times he's been attacked.
     * @param terrain
     * @return
     */
    @Override
    public int getResistanceBonus(Terrain terrain) {
        int resistanceBonus = 0;
        if (timesAttacked == 0) {
            resistanceBonus = 6;
        } else if (timesAttacked == 1) {
            resistanceBonus = 4;
        }else {
            resistanceBonus = 2;
        }
        timesAttacked++;
        return resistanceBonus;
    }

}
