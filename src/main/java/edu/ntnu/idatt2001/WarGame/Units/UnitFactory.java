package edu.ntnu.idatt2001.WarGame.Units;

import edu.ntnu.idatt2001.WarGame.ENUMS.Unit_Type;

import java.util.ArrayList;

public class UnitFactory {
    /**
     * Creates a new unit depending on the unit type sent in by the user.
     * @param unitType
     * @param nameOfUnit
     * @param healthOfUnit
     * @return
     * @throws IllegalArgumentException
     */
    public Unit createUnit(Unit_Type unitType, String nameOfUnit, int healthOfUnit) throws IllegalArgumentException {

        Unit newUnit = switch (unitType) {
            case RANGED_UNIT -> new RangedUnit(nameOfUnit, healthOfUnit);
            case CAVALRY_UNIT -> new CavalryUnit(nameOfUnit, healthOfUnit);
            case INFANTRY_UNIT -> new InfantryUnit(nameOfUnit, healthOfUnit);
            case COMMANDER_UNIT -> new CommanderUnit(nameOfUnit, healthOfUnit);
            default -> throw new IllegalArgumentException("Was unable to read objects classname using" + unitType + "'");
        };
        return newUnit;
    }

    public ArrayList<Unit> createListOfUnits(int numberOfUnits, Unit_Type unitType, String nameOfUnit, int healthOfUnit) {
        ArrayList<Unit> listOfUnits = new ArrayList<>();
        for (int i= 0; i < numberOfUnits; i++) {
            listOfUnits.add(createUnit(unitType, nameOfUnit, healthOfUnit));
        }
        return listOfUnits;
    }
}
