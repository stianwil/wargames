package edu.ntnu.idatt2001.WarGame.Units;

import edu.ntnu.idatt2001.WarGame.ENUMS.Terrain;

import java.util.Objects;

/**
 * The type Unit.
 */
public abstract class Unit {

    private final String name;
    private int health;
    private int attack;
    private int armor;

    /**
     * Instantiates a new Unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     */
    public Unit(String name, int health, int attack, int armor) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;

        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be blank");
        }

        if (health < 0) {
            throw new IllegalArgumentException("Health cannot be lower than 0.");
        }

        if (attack < 0) {
            throw new IllegalArgumentException("Attack stats cannot be lower than 0.");
        }

        if (armor < 0) {
            throw new IllegalArgumentException("Armor stats cannot be lower than 0.");
        }
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets health.
     *
     * @return the health
     */
    public int getHealth() {
        return health;
    }

    /**
     * Sets health.
     *
     * @param health the health
     */
    public void setHealth(int health) {
        this.health = Math.max(health, 0);
    }

    /**
     * Gets attack.
     *
     * @return the attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Gets armor.
     *
     * @return the armor
     */
    public int getArmor() {
        return armor;
    }


    /**
     * Gets attack bonus.
     *
     * @return the attack bonus
     */
    public abstract int getAttackBonus(Terrain terrain);

    /**
     * Gets resistance bonus.
     *
     * @return the resistance bonus
     */
    public abstract int getResistanceBonus(Terrain terrain);

    /**
     * Attack.
     *
     * @param opponent the opponent
     */
    public void attack(Unit opponent, Terrain terrain) {
       opponent.setHealth(opponent.getHealth() - (getAttack() + getAttackBonus(terrain)) + (getArmor() + opponent.getResistanceBonus(terrain))); ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unit unit = (Unit) o;
        return health == unit.health && attack == unit.attack && armor == unit.armor && Objects.equals(name, unit.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, health, attack, armor);
    }

    @Override
    public String toString() {

        return name + " - Health: " + health + '\n';
    }

}
