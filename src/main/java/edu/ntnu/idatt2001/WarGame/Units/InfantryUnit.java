package edu.ntnu.idatt2001.WarGame.Units;

import edu.ntnu.idatt2001.WarGame.ENUMS.Terrain;

/**
 * The type Infantry unit.
 */
public class InfantryUnit extends Unit {

    /**
     * Instantiates a new Infantry unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Instantiates a new Infantry unit.
     *
     * @param name   the name
     * @param health the health
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    /**
     * Gets bonus attack damage if fighting in the forest terrain.
     * @param terrain
     * @return
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int attackBonus = 3;
        if (terrain.equals(Terrain.FOREST)) {
            attackBonus += 2;
        }
        return attackBonus;
    }

    /**
     * Gets bonus resistance if fighting in the forest.
     * @param terrain
     * @return
     */
    @Override
    public int getResistanceBonus(Terrain terrain) {
        int resistanceBonus = 2;
        if (terrain.equals(Terrain.FOREST)) {
            resistanceBonus += 3;
        }
        return resistanceBonus;
    }

}
