package edu.ntnu.idatt2001.WarGame.Units;
import edu.ntnu.idatt2001.WarGame.ENUMS.Terrain;


/**
 * The type Cavalry unit.
 */
public class CavalryUnit extends Unit{

    private boolean hasAttacked;

    /**
     * Instantiates a new Cavalry unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
        this.hasAttacked = false;
    }

    /**
     * Instantiates a new Cavalry unit.
     *
     * @param name   the name
     * @param health the health
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
        this.hasAttacked = false;
    }

    /**
     * The attack bonus of the cavalry unit.
     * If he has not attacked before, his attack will deal bonus damage.
     * Also if he attacks in the plains, he will deal 2 bonus damage.
     * @param terrain
     * @return
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int attackBonus = 2;
        if (hasAttacked == false) {
            hasAttacked = true;
            return attackBonus+4;
        }

        if (terrain.equals(Terrain.PLAINS)) {
            attackBonus += 2;
        }
        return attackBonus;
    }

    /**
     * Will lose his bonus resistance if fought in the forest.
     * @param terrain
     * @return
     */
    @Override
    public int getResistanceBonus(Terrain terrain) {
        int resistanceBonus = 1;

        if (terrain.equals(Terrain.FOREST)) {
            resistanceBonus = 0;
        }
        return resistanceBonus;
        }


}
