package edu.ntnu.idatt2001.WarGame.ENUMS;

public enum Unit_Type {
    CAVALRY_UNIT,
    COMMANDER_UNIT,
    INFANTRY_UNIT,
    RANGED_UNIT;

    public static Unit_Type getUnitType(String unitType) {
        switch (unitType) {
            case "InfantryUnit": return INFANTRY_UNIT;

            case "CommanderUnit": return COMMANDER_UNIT;

            case "CavalryUnit": return CAVALRY_UNIT;

            case "RangedUnit": return RANGED_UNIT;

            default: throw new IllegalArgumentException("Was unable to read objects classname using" + unitType + "'");
        }
    }
}
