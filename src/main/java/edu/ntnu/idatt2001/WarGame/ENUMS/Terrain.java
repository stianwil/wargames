package edu.ntnu.idatt2001.WarGame.ENUMS;

import java.util.Random;

public enum Terrain {
    HILL,
    PLAINS,
    FOREST;

    public static Terrain getTerrain(String terrain) {
        return switch (terrain) {
            case "Hill" -> HILL;
            case "Plains" -> PLAINS;
            case "Forest" -> FOREST;
            default -> null;
        };
    }
}
