package edu.ntnu.idatt2001.WarGame.FileManagement;

import edu.ntnu.idatt2001.WarGame.Army.Army;
import edu.ntnu.idatt2001.WarGame.Units.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This is a class to read and write army objects to a file
 * An army will be written to a csv-file, and will be written on this format:
 *
 * <pre>{@code}
 * Army name
 * unit type, unit name, unit health
 * </pre>
 *
 * Example code:
 * <pre>{@code}
 * Human army
 * InfantryUnit, Human, 100
 * InfantryUnit, Human, 100
 * InfantryUnit, Human, 100
 * </pre>
 */


public class FileManagement {
    private static final String COMMA = ",";
    private static final String NEWLINE = "\n";
    private static final String FILE_ENDING = ".csv";


    public static void writeArmyToFile(File file, Army army) throws IOException {
        if (!file.getName().endsWith(FILE_ENDING)) {
            throw new IOException("File writing for an army is only supported by " + FILE_ENDING + "files.");
        }

        ArrayList<Unit> units = army.getAllUnits();
        StringBuilder writeLine = new StringBuilder();

        try (FileWriter fileWriter = new FileWriter(file)) {
            writeLine.append(army.getName()).append(NEWLINE);
            units.forEach(s -> writeLine.append(s.getClass().getSimpleName()).append(COMMA)
                    .append(s.getName()).append(COMMA)
                    .append(s.getHealth()).append(NEWLINE));
            fileWriter.write(writeLine.toString());
        } catch (IOException ioException) {
            throw new IOException("Could not write this army to the file with name " + file.getName()
            + "due to " + ioException.getMessage());
        }
    }


    public static Army readArmyFromFile(File file) throws IOException, IllegalArgumentException {
        if (!file.getName().endsWith(FILE_ENDING)) {
            throw new IOException("File writing for an army supports only " + FILE_ENDING + " files.");
        }
        String armyName = "";
        ArrayList<Unit> units = new ArrayList<>();
        Unit unit;
        String[] separatedLine;
        String unitClass;
        String unitName;
        int unitHealth;

        try (Scanner scan = new Scanner(file)) {
            armyName = scan.nextLine();

            while (scan.hasNext()) {
                String line = scan.nextLine();
                separatedLine = line.split(COMMA);
                if (separatedLine.length!=3) {
                    throw new IOException("The army could not be read, due to an error in the written file.");
                }
                unitClass = separatedLine[0];
                unitName = separatedLine[1];

                try {
                    unitHealth = Integer.parseInt(separatedLine[2]);
                } catch (NumberFormatException numberFormatException) {
                    throw new NumberFormatException("The units health could not be processed, due to it not being an integer");
                }

                unit = switch (unitClass) {
                    case "InfantryUnit" -> new InfantryUnit(unitName, unitHealth);
                    case "RangedUnit" -> new RangedUnit(unitName, unitHealth);
                    case "CavalryUnit" -> new CavalryUnit(unitName, unitHealth);
                    case "CommanderUnit" -> new CommanderUnit(unitName, unitHealth);
                    default -> throw new IOException("Could not read the units class");
                };
                units.add(unit);
            }

            return new Army(armyName, units);

        } catch (IOException ioException) {
            throw new IOException("The info in the file could not be read because of: "+ ioException.getMessage());
        } catch (NumberFormatException numberFormatException) {
            throw new NumberFormatException("Could not create a unit from the info because of: " + numberFormatException.getMessage());
        } catch (IllegalArgumentException illegalArgumentException) {
            throw new IllegalArgumentException("Could not recreate the unit because of: " + illegalArgumentException);
        }
    }

}
