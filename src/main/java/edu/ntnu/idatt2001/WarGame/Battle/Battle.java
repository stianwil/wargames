package edu.ntnu.idatt2001.WarGame.Battle;

import edu.ntnu.idatt2001.WarGame.Army.Army;
import edu.ntnu.idatt2001.WarGame.ENUMS.Terrain;
import edu.ntnu.idatt2001.WarGame.Units.Unit;


/**
 * The Battle class.
 */

public class Battle {
    private final Terrain terrain;
    private Army armyOne;
    private Army armyTwo;

    /**
     * Instantiates a new Battle.
     *
     * @param armyOne First army
     * @param armyTwo Second army
     * @param terrain Terrain to be fought in.
     */
    public Battle(Army armyOne, Army armyTwo, Terrain terrain) {
        if (!armyOne.hasUnits() || !armyTwo.hasUnits()) {
        throw new IllegalArgumentException("An army cannot be empty.");
        }
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrain;
    }

    /**
     * Simulate a battle between the armies.
     * Runs a while loop as long as both armies contains units.
     * Then gets a random unit to attack another random unit.
     * Every other attack goes between each army.
     * Will in the end return the winning army which still contains units.
     *
     * @return the winning army
     */
    public Army simulate() {
        int counter = 0;
        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            Army attackingArmy = armyOne;
            Army defendingArmy = armyTwo;

            if (counter % 2 == 0) {
                attackingArmy = armyTwo;
                defendingArmy = armyOne;
            }
            Unit attackingUnit = attackingArmy.getRandom();
            Unit defendingUnit = defendingArmy.getRandom();

            attackingUnit.attack(defendingUnit, terrain);

            defendingArmy.removeDeadUnit(defendingUnit);



            counter++;
        }

        if (!armyOne.hasUnits()) {
            return armyTwo;
        } else {
            return armyOne;
        }
    }

    /**
     * toString to return a battle between to armies and its units.
     * @return
     */
    @Override
    public String toString() {
        return "Battle{" +
                "armyOne=" + armyOne +
                ", armyTwo=" + armyTwo +
                '}';
    }
}
