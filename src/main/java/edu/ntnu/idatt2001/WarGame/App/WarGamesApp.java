package edu.ntnu.idatt2001.WarGame.App;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class WarGamesApp extends Application {

    /**
     * Sets the stage for the program to load in.
     * Sets the title of the scene as "War Games".
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/StartPage.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("War Games");
        stage.setResizable(false);
        stage.show();
    }
}
