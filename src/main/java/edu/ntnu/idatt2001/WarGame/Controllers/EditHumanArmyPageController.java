package edu.ntnu.idatt2001.WarGame.Controllers;

import edu.ntnu.idatt2001.WarGame.App.Alertbox;
import edu.ntnu.idatt2001.WarGame.Army.Army;
import edu.ntnu.idatt2001.WarGame.ENUMS.Unit_Type;
import edu.ntnu.idatt2001.WarGame.Units.CommanderUnit;
import edu.ntnu.idatt2001.WarGame.Units.Unit;
import edu.ntnu.idatt2001.WarGame.Units.UnitFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static edu.ntnu.idatt2001.WarGame.FileManagement.FileManagement.readArmyFromFile;
import static edu.ntnu.idatt2001.WarGame.FileManagement.FileManagement.writeArmyToFile;

public class EditHumanArmyPageController implements Initializable {

    @FXML
    public ListView<String> currentArmyList;
    public ListView<String> commanderInfo;
    public ListView<String> cavalryInfo;
    public ListView<String> rangedInfo;
    public ListView<String> infantryInfo;
    public CheckBox commanderCbox;
    public CheckBox cavalryCbox;
    public CheckBox rangedCbox;
    public CheckBox infantryCbox;
    public TextField txtUnitAmount;
    public TextField txtUnitName;
    public TextField txtUnitAmountToRemove;
    public ChoiceBox<String> unitRemoveChoicebox;


    private String[] unit_types = {"CommanderUnit", "CavalryUnit", "InfantryUnit", "RangedUnit"};
    private ArrayList<String> unitsInArmy;
    public Army currentHumanArmy;
    public Unit CommanderUnit;
    public Unit RangedUnit;
    public Unit CavalryUnit;
    public Unit InfantryUnit;
    private Stage stage;
    private Scene scene;

    /**
     * method runs when scene is initialized.
     * Reads the current human army page and displays unit information for user to see stats.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        unitsInArmy = new ArrayList<>();
        unitRemoveChoicebox.getItems().addAll(unit_types);
        try {
            currentHumanArmy = readArmyFromFile(new File("src/main/resources/Files/humanArmy.csv"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        displayCurrentHumanArmy();
        getUnitInformation();
    }

    /**
     * Displays the current human army and which units it contains.
     */
    public void displayCurrentHumanArmy() {
        currentArmyList.getItems().clear();
        currentArmyList.getItems().add("Total amount of Units in your Army: " + currentHumanArmy.getAllUnits().size() + "x");
        currentArmyList.getItems().add("Amount of Commander Units: " + currentHumanArmy.getCommanderUnit().size() + "x");
        currentArmyList.getItems().add("Amount of Ranged Units: " + currentHumanArmy.getRangedUnit().size() + "x");
        currentArmyList.getItems().add("Amount of Cavalry Units: " + currentHumanArmy.getCavalryUnit().size() + "x");
        currentArmyList.getItems().add("Amount of Infantry Units: " + currentHumanArmy.getInfantryUnit().size() + "x");
    }

    /**
     * Adds units depending on what checkbox is selected. If there is no unittype selected, the user will be
     * prompted with an alertbox.
     * Adds the amount of units chosen by user.
     * Will then update the current army list.
     * @param event
     * @throws IOException
     */
    public void btnAddUnits(javafx.event.ActionEvent event) throws IOException {
        UnitFactory newUnit = new UnitFactory();
        Unit_Type  unitType = null;
        String nameOfUnit;
        int amountOfUnits = 0;
        int healthOfUnit;
        if (commanderCbox.isSelected()) {
            unitType = Unit_Type.COMMANDER_UNIT;
        } else if (cavalryCbox.isSelected()) {
            unitType = Unit_Type.CAVALRY_UNIT;
        } else if (rangedCbox.isSelected()) {
            unitType = Unit_Type.RANGED_UNIT;
        } else if (infantryCbox.isSelected()) {
            unitType = Unit_Type.INFANTRY_UNIT;
        } else {
            Alertbox.display("Fail", "You have to select a type of unit");
        }
        
        nameOfUnit = txtUnitName.getText();
        amountOfUnits = Integer.parseInt(txtUnitAmount.getText());


        for (int i = 0; i < amountOfUnits; i++) {
            currentHumanArmy.addUnit(newUnit.createUnit(unitType, nameOfUnit, 100));
        }

        writeArmyToFile(new File("src/main/resources/Files/humanArmy.csv"), currentHumanArmy);
        displayCurrentHumanArmy();

    }

    /**
     * Removes unit depending on which unittype is selected in the choicebox.
     * Then writes the army to the army file.
     * @param event
     * @throws IOException
     */
    public void btnRemoveUnit(ActionEvent event) throws IOException {

        int amountOfUnits = Integer.parseInt(txtUnitAmountToRemove.getText());
        ArrayList<Unit>listOfUnitsToBeRemoved = new ArrayList<>();
        String currentUnitType = unitRemoveChoicebox.getValue();

        if (Unit_Type.getUnitType(currentUnitType).equals(Unit_Type.COMMANDER_UNIT)) {
            listOfUnitsToBeRemoved = currentHumanArmy.getCommanderUnit();
        }

        if (Unit_Type.getUnitType(currentUnitType).equals(Unit_Type.CAVALRY_UNIT)) {
            listOfUnitsToBeRemoved = currentHumanArmy.getCavalryUnit();
        }

        if (Unit_Type.getUnitType(currentUnitType).equals(Unit_Type.INFANTRY_UNIT)) {
            listOfUnitsToBeRemoved = currentHumanArmy.getInfantryUnit();
        }

        if (Unit_Type.getUnitType(currentUnitType).equals(Unit_Type.RANGED_UNIT)) {
            listOfUnitsToBeRemoved = currentHumanArmy.getRangedUnit();
        }

        if (amountOfUnits > listOfUnitsToBeRemoved.size()) {
            Alertbox.display("Fail", "Cant remove more units than in the army.");
        } else{

            for (int i = 0; i < amountOfUnits; i++) {
                currentHumanArmy.removeUnit(listOfUnitsToBeRemoved.get(i));
            }
        }

        writeArmyToFile(new File("src/main/resources/Files/humanArmy.csv"), currentHumanArmy);
        displayCurrentHumanArmy();

    }

    /**
     * Method to display unit information such as attack damage etc.
     */
    private void getUnitInformation() {
        String commanderInformationString = "Commander" + "\n" + "\n"
                + "Attack Damage: 25" + "\n"
                + "Health: 100" + "\n"
                + "Armor: 15";
        commanderInfo.getItems().add(commanderInformationString);

        String cavalryInformationString = "Cavalry" + "\n" + "\n"
                + "Attack Damage: 20" + "\n"
                + "Health: 100" + "\n"
                + "Armor: 12";
        cavalryInfo.getItems().add(cavalryInformationString);

        String rangedInformationString = "Ranged" + "\n" + "\n"
                + "Attack Damage: 15" + "\n"
                + "Health: 100" + "\n"
                + "Armor: 8";
        rangedInfo.getItems().add(rangedInformationString);

        String infantryInformationString = "Infantry" + "\n" + "\n"
                + "Attack Damage: 15" + "\n"
                + "Health: 100" + "\n"
                + "Armor: 10";
        infantryInfo.getItems().add(infantryInformationString);

    }

    /**
     * Switches the menu page.
     * @param event
     * @throws IOException
     */
    public void switchToMenuPage(javafx.event.ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/Menu.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
