package edu.ntnu.idatt2001.WarGame.Controllers;

import edu.ntnu.idatt2001.WarGame.App.Alertbox;
import edu.ntnu.idatt2001.WarGame.Army.Army;
import edu.ntnu.idatt2001.WarGame.Battle.Battle;
import edu.ntnu.idatt2001.WarGame.ENUMS.Terrain;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static edu.ntnu.idatt2001.WarGame.FileManagement.FileManagement.readArmyFromFile;
import static edu.ntnu.idatt2001.WarGame.FileManagement.FileManagement.writeArmyToFile;

public class BattleResultController implements Initializable {

    private String[] terrainTypes = {"Hill", "Plains", "Forest"};
    private Stage stage;
    private Scene scene;
    public Army currentHumanArmy;
    public Army currentOrcArmy;
    public Battle battle;

    @FXML
    public ListView<String> humanArmyList;
    public ListView<String> orcArmyList;
    public ListView<String> winningList;
    public ChoiceBox<String> terrainChoicebox;
    public Label winningArmy;

    /**
     * Method which runs when the page is initialized.
     * Reads the human and orc armies and then prints them in a listview using displayBothArmies().
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            currentHumanArmy = readArmyFromFile(new File("src/main/resources/Files/humanArmy.csv"));
            currentOrcArmy = readArmyFromFile(new File("src/main/resources/Files/orcArmy.csv"));
            writeToBackUpArmies();
        } catch (IOException e) {
            e.printStackTrace();
        }
        displayBothArmies();
        terrainChoicebox.getItems().addAll(terrainTypes);

    }

    /**
     * Method runs when start battle button is pressed.
     * Will then instantiate a new battle between the human and orc armies.
     * A label will display showing the winning army.
     * Then the winning list will be updated and show the suriving units in the winning army.
     * @param event
     * @throws IOException
     */
    public void startBattleButton(javafx.event.ActionEvent event) throws IOException{
        if (!currentOrcArmy.hasUnits() || !currentHumanArmy.hasUnits()) {
            Alertbox.display("Fail", "Cant start a war with an empty army!");
        } else{
            Terrain selectedTerrain = Terrain.getTerrain(terrainChoicebox.getValue());
            battle = new Battle(currentHumanArmy, currentOrcArmy, selectedTerrain);
            battle.simulate();
            writeArmyToFile(new File("src/main/resources/Files/humanArmy.csv"), currentHumanArmy);
            writeArmyToFile(new File("src/main/resources/Files/orcArmy.csv"), currentOrcArmy);
            currentHumanArmy = readArmyFromFile(new File("src/main/resources/Files/humanArmy.csv"));
            currentOrcArmy = readArmyFromFile(new File("src/main/resources/Files/orcArmy.csv"));
            displayBothArmies();
            if (!currentHumanArmy.hasUnits()) {
                winningArmy.setText("Orc Army!");
            } else {
                winningArmy.setText("Human Army!");
            }

            updateWinningList();
        }
    }

    /**
     * Method to quick reset the armies so you dont have to create the armies at new all the time.
     * @param event
     * @throws IOException
     */
    public void resetArmies(javafx.event.ActionEvent event) throws IOException{
        currentHumanArmy = readArmyFromFile(new File("src/main/resources/Files/quickResetHumanArmy.csv"));
        currentOrcArmy = readArmyFromFile(new File("src/main/resources/Files/quickResetOrcArmy.csv"));
        writeArmyToFile(new File("src/main/resources/Files/humanArmy.csv"), currentHumanArmy);
        writeArmyToFile(new File("src/main/resources/Files/orcArmy.csv"), currentOrcArmy);
        displayBothArmies();
        winningList.getItems().clear();
    }

    /**
     * Displays your current armies in the listviews.
     */
    private void displayBothArmies() {
        humanArmyList.getItems().clear();
        humanArmyList.getItems().add("Total amount of Units in your Army: " + currentHumanArmy.getAllUnits().size() + "x");
        humanArmyList.getItems().add("Amount of Commander Units: " + currentHumanArmy.getCommanderUnit().size() + "x");
        humanArmyList.getItems().add("Amount of Ranged Units: " + currentHumanArmy.getRangedUnit().size() + "x");
        humanArmyList.getItems().add("Amount of Cavalry Units: " + currentHumanArmy.getCavalryUnit().size() + "x");
        humanArmyList.getItems().add("Amount of Infantry Units: " + currentHumanArmy.getInfantryUnit().size() + "x");

        orcArmyList.getItems().clear();
        orcArmyList.getItems().add("Total amount of Units in your Army: " + currentOrcArmy.getAllUnits().size() + "x");
        orcArmyList.getItems().add("Amount of Commander Units: " + currentOrcArmy.getCommanderUnit().size() + "x");
        orcArmyList.getItems().add("Amount of Ranged Units: " + currentOrcArmy.getRangedUnit().size() + "x");
        orcArmyList.getItems().add("Amount of Cavalry Units: " + currentOrcArmy.getCavalryUnit().size() + "x");
        orcArmyList.getItems().add("Amount of Infantry Units: " + currentOrcArmy.getInfantryUnit().size() + "x");
    }

    /**
     * Saves the current armies to the quickreset files so you can reset the armies easily.
     * @throws IOException
     */
    private void writeToBackUpArmies() throws IOException {
        writeArmyToFile(new File("src/main/resources/Files/quickResetHumanArmy.csv"), currentHumanArmy);
        writeArmyToFile(new File("src/main/resources/Files/quickResetOrcArmy.csv"), currentOrcArmy);
    }

    /**
     * Switches to the menu page.
     * @param event
     * @throws IOException
     */
    public void switchToMenuPage(javafx.event.ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/Menu.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Prints all surviving units of the winning army in a listview.
     */
    private void updateWinningList() {
        winningList.getItems().clear();
        if (!currentHumanArmy.hasUnits()) {
            winningList.getItems().add(currentOrcArmy.toString());
        } else {
            winningList.getItems().add(currentHumanArmy.toString());
        }
    }


}
