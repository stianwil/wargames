package edu.ntnu.idatt2001.WarGame.Controllers;

import edu.ntnu.idatt2001.WarGame.Army.Army;
import edu.ntnu.idatt2001.WarGame.Battle.Battle;
import edu.ntnu.idatt2001.WarGame.ENUMS.Terrain;
import edu.ntnu.idatt2001.WarGame.FileManagement.FileManagement;
import edu.ntnu.idatt2001.WarGame.Units.Unit;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static edu.ntnu.idatt2001.WarGame.FileManagement.FileManagement.readArmyFromFile;

public class MenuController implements Initializable {

    private String[] terrainTypes = {"Hill", "Plains", "Forest"};
    private Stage stage;
    private Scene scene;
    public Army currentHumanArmy;
    public Army currentOrcArmy;
    public Battle battle;

    @FXML
    public ListView<String> humanArmyList;
    public ListView<String> orcArmyList;


    /**
     * Reads the armies when page is loaded and will display them.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            currentHumanArmy = readArmyFromFile(new File("src/main/resources/Files/humanArmy.csv"));
            currentOrcArmy = readArmyFromFile(new File("src/main/resources/Files/orcArmy.csv"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        displayBothArmies();
    }

    /**
     * Displays the current units registered in the armies.
     */
    public void displayBothArmies() {
        humanArmyList.getItems().clear();
        humanArmyList.getItems().add("Total amount of Units in your Army: " + currentHumanArmy.getAllUnits().size() + "x");
        humanArmyList.getItems().add("Amount of Commander Units: " + currentHumanArmy.getCommanderUnit().size() + "x");
        humanArmyList.getItems().add("Amount of Ranged Units: " + currentHumanArmy.getRangedUnit().size() + "x");
        humanArmyList.getItems().add("Amount of Cavalry Units: " + currentHumanArmy.getCavalryUnit().size() + "x");
        humanArmyList.getItems().add("Amount of Infantry Units: " + currentHumanArmy.getInfantryUnit().size() + "x");

        orcArmyList.getItems().clear();
        orcArmyList.getItems().add("Total amount of Units in your Army: " + currentOrcArmy.getAllUnits().size() + "x");
        orcArmyList.getItems().add("Amount of Commander Units: " + currentOrcArmy.getCommanderUnit().size() + "x");
        orcArmyList.getItems().add("Amount of Ranged Units: " + currentOrcArmy.getRangedUnit().size() + "x");
        orcArmyList.getItems().add("Amount of Cavalry Units: " + currentOrcArmy.getCavalryUnit().size() + "x");
        orcArmyList.getItems().add("Amount of Infantry Units: " + currentOrcArmy.getInfantryUnit().size() + "x");
    }


    /**
     * Switches to edit human army page.
     * @param event
     * @throws IOException
     */
    public void switchToEditHumanArmyPage(javafx.event.ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/EditHumanArmyPage.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Switches the edit orc army page.
     * @param event
     * @throws IOException
     */
    public void switchToEditOrcArmyPage(javafx.event.ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/EditOrcArmyPage.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Switches to the battle page.
     * @param event
     * @throws IOException
     */
    public void switchToBattlePage(javafx.event.ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/BattleResult.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Switches to the start up page.
     * @param event
     * @throws IOException
     */
    public void switchToHomePage(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/StartPage.fxml"));
        stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
