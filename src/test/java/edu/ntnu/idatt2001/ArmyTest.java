package edu.ntnu.idatt2001;
import edu.ntnu.idatt2001.WarGame.Army.*;
import edu.ntnu.idatt2001.WarGame.ENUMS.Terrain;
import edu.ntnu.idatt2001.WarGame.ENUMS.Unit_Type;
import edu.ntnu.idatt2001.WarGame.Units.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class ArmyTest {
    Army testArmy = new Army("Test Army");
    ArrayList<Unit> testUnit = new ArrayList<>();

    @Nested
    class ArmyNameTest {
        @Test
        public void doesConstructorThrowExceptionWhenNameIsBlank() {
            assertThrows(IllegalArgumentException.class,()-> new Army(""));
        }
    };


    @Nested
    class addUnitTest {
        @Test
        public void doesAddUnitMethodThrowUnitIfHealthIsZero() {
            assertThrows(IllegalArgumentException.class,()->new InfantryUnit("Infantry", -2));
        }
    }

    @Nested
    class addAllUnits {
        @Test
        public void addAllUnits(){
            ArrayList<Unit> unitsList = new ArrayList<>();
            for(int i = 0; i<20; i++){
                unitsList.add(new InfantryUnit("Infantry Unit", 100));
            }
            testArmy.addAllUnits(unitsList);
            assertEquals(testArmy.hasUnits(), true);
        }
    }

    @Nested
    class removeUnit {
        @Test
        public void removeUnitTest() {
            Unit unitDontWannaLive = new RangedUnit("Ranged Unit", 1);
            testArmy.addUnit(unitDontWannaLive);
            unitDontWannaLive.attack(unitDontWannaLive, Terrain.getTerrain("Hill"));
            testArmy.removeDeadUnit(unitDontWannaLive);
            assertTrue(testArmy.getAllUnits().size() == 0);
        }
    }

    @Nested
    class getRandomTest {

        @Test
        public void getRandomUnitTest() {
            Unit randomUnit1 = new RangedUnit("RandomOne", 100);
            Unit randomUnit2 = new RangedUnit("RandomTwo", 100);
            Unit randomUnit3 = new RangedUnit("RandomThree", 100);
            testArmy.addUnit(randomUnit1);
            assertTrue(testArmy.getRandom() == randomUnit1 || testArmy.getRandom() == randomUnit2 || testArmy.getRandom() == randomUnit3);
        }

    }

    @Nested
    class streamToGetUnits {

        private void makeArmy() {
                testArmy.addUnit(new RangedUnit("Ranged", 100));
                testArmy.addUnit(new CavalryUnit("Cavalry", 100));
                testArmy.addUnit(new InfantryUnit("Infantry", 100));
                testArmy.addUnit(new CommanderUnit("Commander", 100));
                InfantryUnit testInfantryUnit = new InfantryUnit("Infantry", 100);
                testUnit.add(testInfantryUnit);
        }


        @Test
        public void getArrayListWithSpecificUnitsTest() {
            makeArmy();
            assertEquals(testUnit.get(0), testArmy.getInfantryUnit().get(0));
        }

        @Test
        public void checkIfUnit_TypesAreEqual() {
            String currentUnitType = "RangedUnit";
            assertEquals(Unit_Type.getUnitType(currentUnitType), Unit_Type.RANGED_UNIT);
        }
    }




}
