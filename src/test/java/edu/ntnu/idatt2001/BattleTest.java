package edu.ntnu.idatt2001;
import edu.ntnu.idatt2001.WarGame.Army.*;
import edu.ntnu.idatt2001.WarGame.ENUMS.Terrain;
import edu.ntnu.idatt2001.WarGame.Units.*;
import edu.ntnu.idatt2001.WarGame.Battle.Battle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;
import static org.junit.jupiter.api.Assertions.*;

@Nested
public class BattleTest {
     Army armyOne = new Army("Army One");
     Army armyTwo = new Army("Army Two");


    private void makeArmy() {
        for (int i = 0; i<20; i++) {
            armyOne.addUnit(new RangedUnit("Ranged", 100));
            armyTwo.addUnit(new RangedUnit("Ranged", 100));
        }

        for (int i = 0; i<20; i++) {
            armyOne.addUnit(new CavalryUnit("Cavalry", 100));
            armyTwo.addUnit(new CavalryUnit("Cavalry", 100));
        }

        for (int i = 0; i<20; i++) {
            armyOne.addUnit(new InfantryUnit("Infantry", 100));
            armyTwo.addUnit(new InfantryUnit("Infantry", 100));
        }
        armyOne.addUnit(new CommanderUnit("Commander", 100));
        armyTwo.addUnit(new CommanderUnit("Commander", 100));
    }

    @Test
    public void simulationTest() {
        makeArmy();
        Battle testBattle = new Battle(armyOne, armyTwo, Terrain.getTerrain("Hill"));
        System.out.println(testBattle.simulate());
        assertEquals(armyOne.getClass(), testBattle.simulate().getClass());
    }

    @Test
    public void battleTestIfArmyThrowWorksWhenThereAreNoUnits() {

        assertThrows(IllegalArgumentException.class, () -> new Battle(armyOne, armyTwo, Terrain.getTerrain("Hill")));
    }


}

