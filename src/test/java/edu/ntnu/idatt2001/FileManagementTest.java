package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.WarGame.Army.Army;
import edu.ntnu.idatt2001.WarGame.FileManagement.FileManagement;
import edu.ntnu.idatt2001.WarGame.Units.CavalryUnit;
import edu.ntnu.idatt2001.WarGame.Units.RangedUnit;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

@Nested
public class FileManagementTest {

    @Test
    public void testToSeeIfMethodWriteArmyToFileDoesNotThrowIOException() {
        var testArmy = new Army("Test Army");
        testArmy.addUnit(new CavalryUnit("Footman", 100));
        testArmy.addUnit(new RangedUnit("Footman", 100));

        FileManagement fileManaged = new FileManagement();
        try {
            fileManaged.writeArmyToFile(new File("src/main/resources/ArmyTestFile/testFileWithInfo.csv"), testArmy);
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    public void testToSeeIfWritingToADirectoryThrowsIOException() {
        FileManagement fileManaged = new FileManagement();
        assertThrows(IOException.class, () -> fileManaged.writeArmyToFile(new File("src/main/resources/ArmyTestFile"), new Army("testArmy")));
    }

    @Test
    public void testToSeeIfWritingAFileWithWrongFormatThrowsIOException() {
        FileManagement fileManaged = new FileManagement();
        assertThrows(IOException.class, () -> fileManaged.writeArmyToFile(new File("src/main/resources/ArmyTestFile/testFileWithWrongFormat.txt"), new Army("testArmy")));
    }

    @Test
    public void testToSeeIfReadArmyFromFileMethodWithRightFormatDoesNotThrowIOException() {
        FileManagement fileManaged = new FileManagement();
        try {
            fileManaged.readArmyFromFile(new File("src/main/resources/ArmyTestFile/testFileWithInfo.csv"));
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    public void testToSeeIfAFileWithTooManyCommasThrowsIOException() {
        FileManagement fileManaged = new FileManagement();
        assertThrows(IOException.class, () -> fileManaged.readArmyFromFile(new File("src/main/resources/ArmyTestFile/testFileWithWrongFormatTooManyCommas.csv")));
    }

    @Test
    public void testToSeeIfReadArmyFromFileMethodThrowsIOExceptionWhenFormatIsWrong() {
        FileManagement fileManaged = new FileManagement();
        assertThrows(IOException.class, () -> fileManaged.readArmyFromFile(new File("src/main/resources/ArmyTestFile/testFileWithWrongFormat.txt")));
    }

}
