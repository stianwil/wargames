package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.WarGame.ENUMS.Terrain;
import edu.ntnu.idatt2001.WarGame.Units.*;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UnitTest {

    @Nested
    class registerNewUnit {
        @Test
        public void registerRangerWrongUnitStatsTest() {
            assertThrows(IllegalArgumentException.class, () -> {
                Unit newRangedUnit = new RangedUnit("", -1, -1, -1) {
                };
            });
        }

        @Test
        public void registerCommanderUnitWrongStats() {
            assertThrows(IllegalArgumentException.class, () -> {
                Unit newCommanderUnit = new CommanderUnit("", -1, -1, -1) {
                };
            });
        }


    }


    @Nested
    class CavalryBonusTest {
        Unit CavalryUnit = new CavalryUnit("Cavalry", 100, 15, 8) {
        };

        {
            CavalryUnit = new CavalryUnit("Cavalry2", 100) {
            };
        }

        @Test
        public void IsCavalryBonusCorrect() {
            Terrain terrain = Terrain.getTerrain("Hill");
            assertTrue(CavalryUnit.getResistanceBonus(terrain) == 1);
            assertTrue(CavalryUnit.getAttackBonus(terrain) == 6);
        }
    }

    @Nested
    class RangedUnitTests {
        Unit RangedUnit = new RangedUnit("Ranged", 100, 15, 8) {
        };

        {
            RangedUnit = new RangedUnit("Ranged2", 100) {
            };
        }

        @Test
        public void IsRangedBonusCorrect() {
            Terrain terrain = Terrain.getTerrain("Hill");
            assertTrue(RangedUnit.getAttackBonus(terrain) == 5);
            assertTrue(RangedUnit.getResistanceBonus(terrain) == 6);
        }
    }

    @Nested
    class InfantryUnitTests {
        Unit InfantryUnit = new InfantryUnit("Infantry", 100, 15, 10) {
        };

        {
            InfantryUnit = new InfantryUnit("Infantry2", 100) {
            };
        }

        @Test
        public void IsInfantryBonusCorrect() {
            Terrain terrain = Terrain.getTerrain("Hill");
            assertTrue(InfantryUnit.getAttackBonus(terrain) == 3);
            assertTrue(InfantryUnit.getResistanceBonus(terrain) == 2);
        }

    }

    @Nested
    class attackTest {

        Unit RangedUnit = new RangedUnit("Ranger", 100, 12, 8) {
        };
        Unit CavalryUnit = new CavalryUnit("Cavalry", 100, 15, 8) {
        };

        @Test
        public void testAnAttack() {
            RangedUnit.attack(CavalryUnit, Terrain.getTerrain("Hill"));
            assertTrue(RangedUnit.getHealth() > CavalryUnit.getHealth());
        }
    }
}
